package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class ListViewModel extends AndroidViewModel {
    private MuseumRepository repository;
    private LiveData<List<Item>> allCities;

    public ListViewModel (Application application) {
        super(application);
        CountDownLatch latch = new CountDownLatch(1);
        Thread t = new Thread(){
            public void run(){
                repository = MuseumRepository.get(application);
                allCities = repository.getAllCities();
                latch.countDown();
            }
        };
        t.start();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public  void RestoreList(){
        CountDownLatch latch = new CountDownLatch(1);
        Thread t = new Thread(){
            public void run(){
                repository = MuseumRepository.get(new Application());
                allCities = repository.getAllCities();
                latch.countDown();
            }
        };
        t.start();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    LiveData<List<Item>> getAllCities() {
        return allCities;
    }

    public void deleteCity(long id) {
        repository.deleteCity(id);
    }


}
