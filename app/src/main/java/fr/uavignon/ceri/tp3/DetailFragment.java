package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.util.Set;
import java.util.concurrent.CountDownLatch;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textCityName, textName, textCategory, textDescription, textTimeFrame, textYear, textTech,textBrand,textWorking;
    private ImageView imgWeather;
    private ProgressBar progress;
    private MuseumRepository repository;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        CountDownLatch latch = new CountDownLatch(1);
        Thread t = new Thread(){
            public void run(){
                repository = MuseumRepository.get(new Application());
                latch.countDown();
            }
        };
        t.start();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Get selected city
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String cityID = args.getItemID();
        Log.d(TAG,"selected id="+cityID);
        viewModel.setCity(cityID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textCityName = getView().findViewById(R.id.nameCity);
        textName = getView().findViewById(R.id.editItemName);
        textCategory = getView().findViewById(R.id.editCategory);
        textDescription = getView().findViewById(R.id.editDescription);
        textTimeFrame = getView().findViewById(R.id.editTimeFrame);
        textYear = getView().findViewById(R.id.editYear);
        textTech = getView().findViewById(R.id.editTech);
        textBrand = getView().findViewById(R.id.editBrand);
        textWorking = getView().findViewById(R.id.editWorking);
        imgWeather = getView().findViewById(R.id.iconeWeather);

        progress = getView().findViewById(R.id.progress);

        getView().findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.updateCity();
                Snackbar.make(view, "Interrogation à faire du service web",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
            }
        });

        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getIsLoading().observe(getViewLifecycleOwner(),isLoading ->{
            progress.setVisibility(View.VISIBLE);
            Log.d("MWEIN","chargement en cours");
        });
        viewModel.getCity().observe(getViewLifecycleOwner(),
                city -> {
                    if (city != null) {
                        Log.d(TAG, "observing city view");

                        textCityName.setText(city.getName());
                        textName.setText(city.getName());
                        textDescription.setText(city.getDescription());
                        textYear.setText(String.valueOf(city.getYear()));
                        textBrand.setText(city.getBrand());
                        textWorking.setText(city.getWorking().toString());
                        textTimeFrame.setText(city.getTimeFrame().toString());
                        textCategory.setText(city.getCategories().toString());
                        textTech.setText(city.getTechnicalDetails().toString());
                        if (city.getPictures() != null) {
                            Set<String> kk = city.getPictures().keySet();
                            String key = null;
                            for (String mem : kk) {
                                key = mem;
                                Log.d("IHATEMAPS",key);
                                break;
                            }
                            loadImage(imgWeather, "https://demo-lia.univ-avignon.fr/cerimuseum/items/" + city.getId() + "/images/" + key,city);
                            //https: //demo-lia.univ-avignon.fr/cerimuseum/items/ry8/images/IMG_1593
                            // /items/<itemID>/images/<imageID>
                        }
                    }else{
                        loadImage(imgWeather, "https://demo-lia.univ-avignon.fr/cerimuseum/items/"+city.getId()+"/thumbnail",city);
                    }
                });



    }

    public void loadImage(ImageView view, String url, Item i) {

            if (url != null) {
                Glide.with(view.getContext())
                        .load(url)
                        .error(Glide.with(view).load("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+i.getId()+"/thumbnail"))
                        .into(view);
            }

    }


}