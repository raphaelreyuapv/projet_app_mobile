package fr.uavignon.ceri.tp3.data.webservice;

import java.util.HashMap;
import java.util.List;

public class ItemDetail {
    public final String description=null;
    public final List<String> categories=null;
    public final String name=null;
    public final List<String> timeFrame=null;
    public final String year=null;
    public final String brand=null;
    public final String working=null;
    public final List<String> technicalDetails=null;
    public final HashMap<String,String> pictures=null;
}
