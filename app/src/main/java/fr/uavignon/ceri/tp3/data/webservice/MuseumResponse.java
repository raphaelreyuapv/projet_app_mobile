package fr.uavignon.ceri.tp3.data.webservice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.uavignon.ceri.tp3.data.Item;

public class MuseumResponse {

    public static void transferInfo(MuseumResponse v, Item c) {
        if(v.year!=null) {
            c.setYear(v.year);
        }
        if(v.description!=null) {
            c.setDescription(v.description);
        }
        if(v.name!=null) {
            c.setName(v.name);
        }
        if(v.brand!=null){
            c.setBrand(v.brand);
        }
        if(v.working!=null){
            c.setWorking(v.working);
        }else{
            c.setWorking(false);
        }
        if(v.timeFrame!=null){
            c.setTimeFrame(v.timeFrame);
        }
        if(v.technicalDetails!=null){
            c.setTechnicalDetails(v.technicalDetails);
        }
        if(v.categories!=null){
            c.setCategories(v.categories);
        }
        if(v.pictures!=null){
            c.setPictures(v.pictures);
        }

    }


        public final Integer year=null;
        public final String brand=null;
        public final Boolean working=null;
        public final String name=null;
        public final String description=null;
        public final List<String> timeFrame=null;
        public final List<String> technicalDetails=null;
        public final List<String> categories=null;
        public final Map<String,String> pictures=null;





    /*
               public final Main main=null;
    public final Wind wind=null;
            public static class Main{
                    public final Float temp=null;
                    public final Integer humidity=null;

            }

            public static class Wind{
                    public final Float speed=null;
                    public final Integer deg=null;
            }
            public static class Detail{
                    public final String icon=null;
            }

     */

}