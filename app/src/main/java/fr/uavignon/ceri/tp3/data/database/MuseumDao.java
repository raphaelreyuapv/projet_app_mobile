package fr.uavignon.ceri.tp3.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;

@Dao
public interface MuseumDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item city);

    @Query("DELETE FROM Item")
    void deleteAll();

    @Query("SELECT * from Item ORDER BY name ASC")
    LiveData<List<Item>> getAllCities();

    @Query("SELECT * from Item ORDER BY name ASC")
    List<Item> getSynchrAllCities();

    @Query("DELETE FROM Item WHERE _id = :id")
    void deleteCity(double id);

    @Query("SELECT * FROM Item WHERE _id = :id")
    Item getCityById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item city);

    @Query("SELECT categories FROM Item;")
    List<String> getCatList();
}
