package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.JsonObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.MuseumDao;
import fr.uavignon.ceri.tp3.data.database.MuseumRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.ItemDetail;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.MuseumResponse;
import fr.uavignon.ceri.tp3.data.webservice.MuseumResult;
import fr.uavignon.ceri.tp3.data.webservice.VersionResult;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.MuseumRoomDatabase.databaseWriteExecutor;

public class MuseumRepository {

    private static final String TAG = MuseumRepository.class.getSimpleName();

    private LiveData<List<Item>> allCities;
    private MutableLiveData<Item> selectedCity;
    private List<String> catList;
    private List<Item> itemListfixed;
    private static Float version = 0.0f;
    private MuseumDao cityDao;
    private final OWMInterface api;

    private static volatile MuseumRepository INSTANCE;

    public static Float getVersion() {
        return version;
    }

    public static void setVersion(Float version) {
        MuseumRepository.version = version;
    }

    public synchronized static MuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository(application);
        }

        return INSTANCE;
    }

    public MuseumRepository(Application application) {
        MuseumRoomDatabase db = MuseumRoomDatabase.getDatabase(application);
        cityDao = db.museumDao();
        allCities = cityDao.getAllCities();
        selectedCity = new MutableLiveData<>();
        isLoading = new MutableLiveData<>();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .build();

        api = retrofit.create(OWMInterface.class);
        getVer();
        Float localVer=getLocalVer();
        Log.d("MWEIN_REMOTE",Float.toString(version));
        Log.d("MWEIN_LOCAL",Float.toString(localVer));
        if(version!=localVer){
            Log.d("MWEIN","updating collection");
                loadCollection();
        }
        itemListfixed = cityDao.getSynchrAllCities();
    }

    public static void writeVer(Float ver){
        Context context = GlobalApplicationContext.getAppContext();
        File path = context.getFilesDir();
        File file = new File(path,"vernum.txt");
        FileOutputStream stream = null;
        try {
            stream = new FileOutputStream(file);
            stream.write(ver.toString().getBytes());
            stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static Float getLocalVer(){
        Context context = GlobalApplicationContext.getAppContext();
        File path = context.getFilesDir();
        File file = new File(path,"vernum.txt");
        int length = (int) file.length();

        byte[] bytes = new byte[length];

        FileInputStream in = null;
        try {
            in=new FileInputStream(file);
            in.read(bytes);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String contents = new String(bytes);
        Float vernum=null;
        try {
            vernum = Float.parseFloat(contents);
        }catch(java.lang.NumberFormatException e){
            vernum=0.0f;

        }
        return vernum;
    }
    public LiveData<List<Item>> getAllCities() {
        return allCities;
    }

    public MutableLiveData<Item> getSelectedCity() {
        return selectedCity;
    }

    public List<String> getCatList() { return catList;}

    public void refreshCatList(){
        catList = cityDao.getCatList();
        itemListfixed = cityDao.getSynchrAllCities();
    }

    public List<Item> getItemListfixed(){
        return itemListfixed;
    }

    public MutableLiveData<Boolean> isLoading;
    public void getVer(){
        final MutableLiveData<VersionResult> result = new MutableLiveData<VersionResult>();
        result.postValue(new VersionResult());

        api.getVersion().enqueue(
                new Callback<Float>(){
                    @Override
                    public void onResponse(Call <Float> call,Response<Float> response){
                        Log.d("MWEIN","all good version check");
                        //setVersion(response.body().vernum);
                        Log.d("MWEINVER","ver is:"+response.body());
                        setVersion(response.body());
                    }

                    @Override
                    public void onFailure(Call <Float> call,Throwable t){
                        Log.d("MWEIN","version check failed");
                        Log.d("MWEIN",t.toString());
                    }
                });


        /*String text = "sometext idk";
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"),text);
        Call<ResponseBody> call = api.getVersion(body);
        Response<ResponseBody> response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String value = response.body().string();
            Log.d("MWEINVER","is this finnaly it:"+value);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
    public void loadItem(Item item){
        isLoading.postValue(true);
        final MutableLiveData<MuseumResult> result = new MutableLiveData<MuseumResult>();
        result.setValue(new MuseumResult(true,null));
            api.getItem(item.getName()).enqueue(
                    new Callback<ItemDetail>(){
                        @Override
                        public void onResponse(Call<ItemDetail> call,
                                               Response<ItemDetail> response){
                            Log.d("MWEIN","all good");
                            isLoading.postValue(false);
                            //result.postValue(new WeatherResult(false,null));
                            //MuseumResult.transferInfo(response.body(),city);
                            updateCity(item);

                        }

                        @Override
                        public void onFailure(Call<ItemDetail> call, Throwable t) {
                            Log.d("zzz",t.toString());
                            isLoading.postValue(false);
                            result.postValue(new MuseumResult(false, null));
                        }
                    });

    }
    public void loadCollection(){
        List<Item> cityList = cityDao.getSynchrAllCities();
        Log.d("MWEIN","updating collection...");
        isLoading.postValue(true);
                Item c = new Item("",0,"");
                final MutableLiveData<MuseumResult> result = new MutableLiveData<MuseumResult>();
                result.postValue(new MuseumResult(true, null));
                api.getList().enqueue(
                        new Callback<Map<String,MuseumResponse>>() {
                            @Override
                            public void onResponse(Call<Map<String,MuseumResponse>> call,
                                                   Response<Map<String,MuseumResponse>> response) {
                                Log.d("MWEIN", "adding new items");
                                //result.postValue(new WeatherResult(false,null));
                                if (response.body() == null){
                                    Log.d("MWEIN","response is null");
                                    Log.d("MWEIN",response.body().toString());
                                }else {
                                    for (Map.Entry<String, MuseumResponse> entry : response.body().entrySet()) {
                                        String k = entry.getKey();
                                        MuseumResponse v = entry.getValue();
                                        c.setId(k);
                                        Log.d("MWEIN",v.name);
                                        MuseumResponse.transferInfo(v, c);
                                        insertCity(c);
                                        Log.d("MWEIN", "item added");
                                    }
                                }

                            }

                            @Override
                            public void onFailure(Call<Map<String,MuseumResponse>>call, Throwable t) {
                                Log.d("MWEIN", t.toString());
                                Log.d("MWEIN","failed to update");
                                //result.postValue(new MuseumResult(false, null));
                            }
                        });

        isLoading.postValue(false);
    }
    public long insertCity(Item newCity) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return cityDao.insert(newCity);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCity);
        return res;
    }

    public int updateCity(Item city) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return cityDao.update(city);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(city);
        return res;
    }

    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            cityDao.deleteCity(id);
        });
    }

    public void getCity(String id)  {
        Future<Item> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return cityDao.getCityById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
