package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.Menu;
import android.view.MenuItem;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update_all) {
            //ListFragmentDirections. action = ListFragmentDirections.actionListFragmentToListCatFragment();
            //Navigation.findNavController(this.findViewById(0)).navigate(action);
            NavController navController = Navigation.findNavController(this,R.id.nav_host_fragment);
            navController.navigate(ListFragmentDirections.actionListFragmentToListCatFragment());
        }

        return super.onOptionsItemSelected(item);
    }
}