package fr.uavignon.ceri.tp3;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;

public class Section {
    private String sectionTitle;
    private List<Item> allItemsInSection;
    public Section(String sectionTitle, List<Item> allItemsInSection) {
        this.sectionTitle = sectionTitle;
        this.allItemsInSection = allItemsInSection;
    }
    public String getSectionTitle() {
        return sectionTitle;
    }
    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }
    public List<Item> getAllItemsInSection() {
        return allItemsInSection;
    }
    public void setAllItemsInSection(List<Item> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }
}
