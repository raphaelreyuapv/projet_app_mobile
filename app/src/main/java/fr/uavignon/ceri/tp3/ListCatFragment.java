package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import fr.uavignon.ceri.tp3.data.Item;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.MuseumRepository;
import fr.uavignon.ceri.tp3.data.database.MuseumRoomDatabase;

public class ListCatFragment extends Fragment {

        public static final String TAG = fr.uavignon.ceri.tp3.ListFragment.class.getSimpleName();

        private ListCatViewModel viewModel;

        private RecyclerView recyclerView;
        private RecyclerView.LayoutManager layoutManager;
        private SectionAdapter adapter;
        private ProgressBar progress;
        private MuseumRepository repository;

        @Override
        public View onCreateView(
                LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState
        ) {
            // Inflate the layout for this fragment
            return inflater.inflate(R.layout.fragment_list, container, false);
        }

        public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            viewModel = new ViewModelProvider(this).get(ListCatViewModel.class);
            listenerSetup();
            observerSetup();

        }

        private void listenerSetup() {
            recyclerView = getView().findViewById(R.id.recyclerView);
            layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            List<Section> sections = new ArrayList<>();
            repository = MuseumRepository.get(new Application());
            CountDownLatch latch = new CountDownLatch(1);
            Thread t = new Thread(){
                public void run(){
                    repository.refreshCatList();
                    latch.countDown();
                }
            };
            t.start();
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //List<String> catList = repository.getCatList();
            List<Item> items = repository.getItemListfixed();
            List<String> uniqueCatList = new ArrayList<String>();
            for(Item i : items){
                for(String s : i.getCategories()) {
                    if (!uniqueCatList.contains(s)) {
                        Log.d("BLABLA", s);
                        uniqueCatList.add(s);
                    }
                }
            }
            for(String s : uniqueCatList){
                List<Item> finalItemList = new ArrayList<Item>();
                for(Item i : items) {
                    //Log.d("MWEIN",i.getName()+"this is me!!!");
                    for(String ss : i.getCategories()) {
                        Log.d("ITEM BEING COMPARED",i.getName());
                        Log.d("COMP",ss);
                        Log.d("COMPD",s);
                        if(s.equals(ss)) {
                            Log.d("ITEM_ADDED",i.getName());
                            Log.d("SECTION_ADDED",s);
                            finalItemList.add(i);
                        }
                    }
                }
                sections.add(new Section(s,finalItemList));
            }
            for(Section s:sections){
                for(Item i:s.getAllItemsInSection()){
                    Log.d("ITEM_IN_SECTION",i.getName());
                    Log.d("SECTION_CONTAINING",s.getSectionTitle());
                }
            }
            adapter = new SectionAdapter(this.getContext(),sections);
            recyclerView.setAdapter(adapter);
            adapter.setListViewModel(viewModel);
            progress = getView().findViewById(R.id.progressList);

            FloatingActionButton fab = getView().findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ListFragmentDirections.ActionListFragmentToNewCityFragment action = ListFragmentDirections.actionListFragmentToNewCityFragment();
                    action.setCityNum(Item.ADD_ID);
                    Navigation.findNavController(view).navigate(action);

                }
            });
        }

        private void observerSetup() {
            viewModel.getAllCities().observe(getViewLifecycleOwner(),
                    cities -> adapter.setItemList(cities));

        }

    }
