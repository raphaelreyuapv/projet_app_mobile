package fr.uavignon.ceri.tp3.data.webservice;

import com.google.gson.JsonObject;
import com.squareup.moshi.Json;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OWMInterface {
    @GET("collection")
    public Call<Map<String,MuseumResponse>> getList();
    @GET("/items/")
    public Call<ItemDetail> getItem(@Query("q") String itemid);
    @GET("/cerimuseum/collectionversion")
    public Call<Float> getVersion();

}
