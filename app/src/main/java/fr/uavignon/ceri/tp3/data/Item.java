package fr.uavignon.ceri.tp3.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(tableName = "Item")
public class Item {

    public static final String TAG = Item.class.getSimpleName();

    public static final long ADD_ID = -1;

    @PrimaryKey(autoGenerate = false)
    @NonNull
    @ColumnInfo(name="_id")
    private String id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="year")
    private int year;


    @ColumnInfo(name="description")
    private String description; // description of the current weather condition (ex: light intensity drizzle)

    @ColumnInfo(name="brand")
    private String brand;

    @ColumnInfo(name="working")
    private Boolean working;

    @ColumnInfo(name="timeFrame")
    @TypeConverters(ListTypeConverter.class)
    private List<String> timeFrame;

    @ColumnInfo(name="technicalDetails")
    @TypeConverters(ListTypeConverter.class)
    private List<String> technicalDetails;

    @ColumnInfo(name="categories")
    @TypeConverters(ListTypeConverter.class)
    private List<String> categories;

    @ColumnInfo(name="pictures")
    @TypeConverters(HashMapTypeConverter.class)
    private Map<String,String> pictures;

    public Item(String name,int year,String description){
        this.name = name;
        this.description = description;
        this.year = year;
    }

    /*public City(@NonNull String name, int year,String description) {
        this.name = name;
        this.description = description;
    }*/

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getYear(){
        return year;
    }

    public void setYear(int year){
        this.year=year;
    }

    public void setBrand(String brand){this.brand=brand;}
    public void setWorking(Boolean working){this.working=working;}
    public void setTimeFrame(List<String> timeFrame){this.timeFrame=timeFrame;}
    public void setTechnicalDetails(List<String> technicalDetails){this.technicalDetails=technicalDetails;}
    public void setCategories(List<String> categories){this.categories=categories;}
    public void setPictures(Map<String,String> pictures){this.pictures=pictures;}
    public String getBrand(){return brand;}
    public Boolean getWorking(){return working;}
    public String getDescription() {
        return description;
    }
    public List<String> getTimeFrame(){return timeFrame;}
    public List<String> getTechnicalDetails(){return technicalDetails;}
    public List<String> getCategories(){return categories;}
    public Map<String,String>getPictures(){return pictures;}


    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) { this.description = description; }

    public boolean search(String searchterm){
        if(id!=null && id.contains(searchterm)){
            return true;
        }
        if(name!=null && (name.contains(searchterm))){
            return true;
        }
        if(description!=null && description.contains(searchterm)){
            return true;
        }
        if(brand!=null && brand.contains(searchterm)){
            return true;
        }
        if(timeFrame!=null && timeFrame.contains(searchterm)){
            return true;
        }
        if(pictures!=null && pictures.containsValue(searchterm)){
            return true;
        }
        if(technicalDetails!=null && technicalDetails.contains(searchterm)){
            return true;
        }
        if(categories!=null && categories.contains(searchterm)){
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return this.name;
    }


}
